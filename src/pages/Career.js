import {
  Button,
  ButtonGroup,
  Fab,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Table,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import SendIcon from "@mui/icons-material/Send";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import Form from "../components/Form";
import { FormControl } from "@mui/material";
import TableC from "../components/Table";
import { TableContainer } from "@mui/material";
import { TableCell } from "@mui/material";
import { TableBody } from "@mui/material";
import { useState } from "react";
import { useEffect } from "react";
import { getCareers } from "../helpers/career";
import { getInstitutes } from "../helpers/institute";

import toast, { Toaster } from "react-hot-toast";

const initialCareer = {
  name: "",
  institute: "",
}

const Career = () => {

  const [career, setCareer] = useState(initialCareer)
  const [careers, setCareers] = useState([])
  const [institutes, setInstitutes] = useState([])
  const [update, setUpdate] = useState(false);

  const updateInstitutes = () => {
    getInstitutes().then((institutesNew) => {
      console.log(institutesNew)
      setInstitutes(institutesNew)
    })
  }


  const updateCareers = () => {
    getCareers().then(careersNew => {
      console.log(careersNew);
      setCareers(careersNew);
    })
  }

  const handleChanguesCareers = (event) => {
    setCareer({ ...career })
  }

  useEffect(() => {
    updateCareers();
    updateInstitutes();
  }, []);


  return (
    <>
      <Form title="Carrera">
        <Grid container spacing={2}>
          <Grid item>
            <TextField
              value={career.name}
              variant="outlined"
              name="name"
              label="Nombre de la carrera" />
          </Grid>
          <Grid item xs={7}>
            <FormControl fullWidth>
              <InputLabel>Instituto</InputLabel>
              <Select label="Instituto">
                <MenuItem value="">
                  <em>Ninguno</em>
                </MenuItem>
                {institutes.map((institute) => (
                  <MenuItem value={institute.id}>{institute.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained">
              <SendIcon />
              {"Enviar"}
            </Button>
          </Grid>
        </Grid>
      </Form>
      <TableC>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Carrera</TableCell>
                <TableCell>Instituto</TableCell>
                <TableCell>Acciones</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {careers.map((career) => (
                <TableRow key={career.id}>
                  <TableCell>{career.name}</TableCell>
                  <TableCell>{career.institute}</TableCell>
                  <TableCell component="th" scope="row">
                    <ButtonGroup variant="text" aria-label="text button group">
                      <Button>
                        {" "}
                        <Fab
                          color="primary"
                          size="small"
                          aria-label="edit"
                        >
                          <EditIcon />
                        </Fab>
                      </Button>
                      <Button>
                        {" "}
                        <Fab
                          color="secondary"
                          size="small"
                          aria-label="edit"
                        >
                          <DeleteIcon />
                        </Fab>
                      </Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </TableC>
    </>
  );
};

export default Career;
