# PRS_Certidigital
En este proyecto de generar la titulación del egresado usando tecnología de blockchain. El team A solo es reponsable de los maestros(Insitutos, carreras y studiantes) y mostrar una UI para que el usuario pueda realizar peticiones(usaremos React)


## Backend Endpoint
Institutos
 http://20.197.186.95:9090/repository/institutes
Carreras
 http://20.197.186.95:9091/repository/careers
Estudiantes
 http://20.197.186.95:9093/repository/users


## Frontend

La carpeta helpers contiene las API para establecer la comunicación con los microservicios y la carpeta pages contiene partes de la web que se juntaran en single spa

## Docker
Commands
- docker build -t vallegrande/react-app .
- docker run -d -it  -p 80:80/tcp --name certidigital-app vallegrande/react-app:latest
